package _112.easyutils.commands;

import _112.easyutils.Utils.Messages;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandFly implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)){
            commandSender.sendMessage(Messages.commandprefix);
            return true;
        }

        Player player = (Player) commandSender;

        if(strings.length <= 0){
            player.setAllowFlight(!player.getAllowFlight());
            commandSender.sendMessage(Messages.commandprefix + "Toggled flight " + (player.getAllowFlight() ? "on" : "off"));
        }
        return true;
    }
}
