package _112.easyutils.commands.Gamemodes;

import _112.easyutils.Utils.CommandPrefix;
import _112.easyutils.Utils.Messages;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandGMA implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)){
            commandSender.sendMessage(Messages.youmustbeaplayer);
        }

        Player player = (Player) commandSender;
        player.setGameMode(GameMode.ADVENTURE);
        player.sendMessage(Messages.gamemodechange + player.getGameMode().toString().toLowerCase());

        return true;
    }
}