package _112.easyutils.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.Objects;

import static _112.easyutils._112Utils.plugin;

public class Command_112U implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(strings.length != 0 && Objects.equals(strings[0], "reload")){
            plugin.saveConfig();
            plugin.reloadConfig();
            commandSender.sendMessage(ChatColor.RED + "Config reloaded!");
        }
        else{
            commandSender.sendMessage( ChatColor.DARK_GRAY +  "==========================" );
            commandSender.sendMessage(ChatColor.GRAY + "_112 util command plugin");
            commandSender.sendMessage("");
            commandSender.sendMessage(ChatColor.GREEN + "/fly " + ChatColor.GRAY + " Enables fly for the sender");
            commandSender.sendMessage(ChatColor.GREEN + "/speed <SPEED> " + ChatColor.GRAY + " Set the fly/walking speed of the sender");
            commandSender.sendMessage(ChatColor.GREEN + "/gc "+ ChatColor.GRAY +" Shows  server tps and other helpful stats");
            commandSender.sendMessage(ChatColor.GREEN + "/gm <GAMEMODE>" + ChatColor.GRAY+" Sets the gamemode to specified");
            commandSender.sendMessage("");
            commandSender.sendMessage(ChatColor.DARK_GRAY +  "==========================" );
        }
        return true;
    }
}
