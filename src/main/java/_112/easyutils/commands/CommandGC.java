package _112.easyutils.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import com.destroystokyo.paper.*;

import java.text.DecimalFormat;

public class CommandGC implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {


        sender.sendMessage(ChatColor.DARK_GRAY +  "==========================" );
        sender.sendMessage(ChatColor.GRAY +  "Current TPS: " + ChatColor.GREEN + String.valueOf(new DecimalFormat("##.#").format(Bukkit.getTPS()[0])));
        sender.sendMessage(ChatColor.GRAY + "Chunks loaded: " + ChatColor.GREEN + String.valueOf(Bukkit.getWorld("world").getLoadedChunks().length));
        sender.sendMessage(ChatColor.GRAY + "Entities loaded: " + ChatColor.GREEN + String.valueOf(Bukkit.getWorld("world").getEntities().size()));
        sender.sendMessage(ChatColor.GRAY + "Tile Entities loaded: " + ChatColor.GREEN + String.valueOf(Bukkit.getWorld("world").getTileEntityCount()));


        for (World world: Bukkit.getWorlds())
             {
                 sender.sendMessage( ChatColor.GRAY + world.getName()+":" + ChatColor.GRAY + " chunks: " + ChatColor.GREEN + String.valueOf(Bukkit.getWorld("world").getLoadedChunks().length)
                         + ChatColor.GRAY + " entities: " + ChatColor.GREEN + String.valueOf(Bukkit.getWorld(world.getName()).getEntities().size())
                         + ChatColor.GRAY + " tiles: " + ChatColor.GREEN + String.valueOf(Bukkit.getWorld(world.getName()).getTileEntityCount()));
        }
        sender.sendMessage(ChatColor.DARK_GRAY +  "==========================" );
        return true;
    }

}
